import { useEffect, useCallback } from "react";

import "./FeedList.css";

export default function FeedList({
  feedItems,
  selectedVideo,
  setActiveVideo,
  setSelectedVideo,
}) {
  const nextIndex = (index) => index + 1;
  const prevIndex = (index) => index - 1;

  const selectVideo = useCallback(
    (fn) => {
      if (feedItems) {
        const currentSelectedIndex = feedItems.indexOf(selectedVideo);

        const index = fn(currentSelectedIndex);

        if (currentSelectedIndex !== -1) {
          const video = feedItems[index];

          if (video) {
            setSelectedVideo(video);
          } else {
            setSelectedVideo(feedItems[0]);
          }
        }
      }
    },
    [selectedVideo, feedItems, setSelectedVideo]
  );

  useEffect(() => {
    document.addEventListener("keydown", (e) => onKeyPress(e));
  }, [selectVideo, setActiveVideo, selectedVideo]);

  const onKeyPress = (e) => {
    if (e.key === "Enter") {
      setActiveVideo(selectedVideo);
    } else if (e.key === "ArrowDown") {
      selectVideo(nextIndex);
    } else if (e.key === "ArrowUp") {
      selectVideo(prevIndex);
    }
  };

  return (
    <div className="feed-list-container">
      <button className="button-primary">UP</button>
      <ul>
        {feedItems &&
          feedItems.map((feed) => (
            <li
              key={feed.guid}
              className={
                feed.guid === selectedVideo.guid ? "activeItemList" : "itemList"
              }
            >
              <h2>{feed.title}</h2>
            </li>
          ))}
      </ul>
      <button className="button-primary">DOWN</button>
    </div>
  );
}
