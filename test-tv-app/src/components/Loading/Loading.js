import "./Loading.css";

export default function Loading() {
  return <div className="loading-wrapper">Loading...</div>;
}
