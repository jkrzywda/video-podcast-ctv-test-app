import { useEffect, useState } from "react";

import FeedList from "../FeedList/FeedList";
import CurrentVideo from "../CurrentVideo/CurrentVideo";
import Loading from "../Loading/Loading";

import { getFeedListing } from "../../requests";

import "./App.css";

function App() {
  const [feed, setFeed] = useState(null);
  const [activeVideo, setActiveVideo] = useState(null);
  const [selectedVideo, setSelectedVideo] = useState(null);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    const feedUrl =
      "http://rss.cnn.com/services/podcasting/studentnews/rss.xml";
    getFeedList(feedUrl);
  }, []);

  const getFeedList = async (url) => {
    setLoading(true);
    try {
      const response = await getFeedListing(url);
      setSelectedVideo(response.data.items[0]);
      setActiveVideo(response.data.items[0]);
      setFeed(response.data.items);
      setError(false);
    } catch (err) {
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  const renderErrorMsg = () => <p>Sorry, something went wrong..</p>;

  const renderFeed = () => (
    <>
      <FeedList
        feedItems={feed}
        selectedVideo={selectedVideo}
        setActiveVideo={setActiveVideo}
        setSelectedVideo={setSelectedVideo}
      />
      <div className="active-video-area-container">
        <CurrentVideo activeVideo={activeVideo} />
      </div>
    </>
  );

  return (
    <div className="App">
      <div className="main-wrapper">
        {loading && <Loading />}
        {error ? renderErrorMsg() : renderFeed()}
      </div>
    </div>
  );
}

export default App;
