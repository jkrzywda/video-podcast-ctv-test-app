import "./CurrentVideo.css";

export default function CurrentVideo({ activeVideo }) {
  return (
    <div>
      {activeVideo && (
        <div>
          <h1>{activeVideo.title}</h1>
          <p>{activeVideo.description}</p>
          <video controls autoPlay>
            <source src={activeVideo.link} type={activeVideo.enclosure.type} />
          </video>
        </div>
      )}
    </div>
  );
}
